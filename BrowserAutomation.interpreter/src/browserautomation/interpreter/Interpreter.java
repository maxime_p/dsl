package browserautomation.interpreter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;

import browserautomation.interpreter.evaluators.BooleanEvaluator;
import browserautomation.interpreter.evaluators.StringEvaluator;
import fr.emn.fila3.browserautomation.bA.Action;
import fr.emn.fila3.browserautomation.bA.Affectation;
import fr.emn.fila3.browserautomation.bA.CallFun;
import fr.emn.fila3.browserautomation.bA.If;
import fr.emn.fila3.browserautomation.bA.Expression;
import fr.emn.fila3.browserautomation.bA.Function;
import fr.emn.fila3.browserautomation.bA.Http;
import fr.emn.fila3.browserautomation.bA.Instruction;
import fr.emn.fila3.browserautomation.bA.Print;
import fr.emn.fila3.browserautomation.bA.Foreach;
import fr.emn.fila3.browserautomation.bA.Program;
import fr.emn.fila3.browserautomation.bA.Selector;
import fr.emn.fila3.browserautomation.bA.Var;
import fr.emn.fila3.browserautomation.bA.While;

public class Interpreter {
	
	public FirefoxProfile profile;
	public FirefoxDriver driver;

	//Map<String, Expression> environment = new HashMap<>();
	Map<String, String> environment = new HashMap<>();
	WebElement currentWebElement;
	String programName;
	
	public Interpreter() {
		profile = new FirefoxProfile();
		driver = new FirefoxDriver(profile);
		driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
	}
	
	// =============== Methodes utilitaires =====================
	
	public void info(String message) {
		System.out.println(message);
	}
	
	public void error(String message) {
		throw new RuntimeException("Interpreter : " + message);
	}
	
	private void displayBAMessage() {
		info("\n" + 
			 "##############################\n" +
			 "##  Browser Automation 1.0  ##\n" +
			 "## ====== EMN-FIL A3 ====== ##\n" +
			 "##  Joris Pichard           ##\n" + 
			 "##     Maxime Perocheau     ##\n" + 
			 "##          Hadrien Gerard  ##\n" + 
			 "##############################\n");
	}
	
	
	
	// =============== "Point d'entree" =========================
	
	public void execute(Program prog) {
		this.displayBAMessage();
		info("Launching program " + prog.getName());
		this.programName = prog.getName();
		if (prog.getMain() != null) {
			info("Calling Main function");
			this.processInstructions(prog.getMain().getIntructions());
		} else {
			error("No 'Main()' function found !");
		}	
	}
	
	
	
	// ===============  Processing  =============================
	
	private void processInstructions(List<Instruction> listIns) {
		for(Instruction ins : listIns){
			if (ins instanceof CallFun){
				processCallFun((CallFun)ins);
			}
			else if (ins instanceof Print) {
				processPrint((Print) ins);
			}
			else if (ins instanceof Http){
				processHttp((Http)ins);
			}
			else if (ins instanceof Action) {
				processAction((Action)ins); 
			} 
			else if (ins instanceof Affectation){
				processAffectation((Affectation)ins);
			} 
			else if (ins instanceof If) {
				processIf((If)ins);
			}
			else if (ins instanceof While){
				processWhile((While)ins);
			}
		}
		
	}	

	private void processCallFun(CallFun cf){
		Function f = cf.getFunction();
		info("Calling function " + f.getName());
		this.processArgs(f.getName(), f.getVars(), cf.getArgs());
		this.processInstructions(f.getIntructions());
	}
	
	private void processArgs(String callName, List<Var> vars, List<Expression> values) {
		
		if (vars==null || values==null || callName==null) {
			error("ProcessArgs => \n\tcallName==null : " + (callName==null) +
				  "\n\tvars==null : " + (vars==null) + 
				  "\n\tvalues==null : " + (values==null));
		} else if (vars.size() != values.size()) {
			error("Erreur dans l'appel � " + callName + 
										" : Le nombre d'arguments ne correspond pas");
		} else if (! vars.isEmpty()) {
			for (int index=0; index < vars.size(); index++) {
				processAffectation(vars.get(index).getName(), values.get(index));
			}	
		}

	}

	private void processPrint(Print print) {
		StringEvaluator stringEvaluator = new StringEvaluator(environment, currentWebElement);
		String strValue = stringEvaluator.evaluateExpression(print.getExpression());
		info(programName + " => " + strValue);
	}
	
	private void processHttp(Http http){
		if(http.getMethod().equals("get")){
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
			}
			driver.get(http.getUrl());
		}
	}
	
	private void processAction(Action action) {
		if(action instanceof Foreach) {
			processForeach((Foreach)action);
		} 
		else if (action.getSelector() != null) {
			List<WebElement> elements = processSelector(action.getSelector());
			
			if (!elements.isEmpty()) {
				WebElement element = elements.get(0);
				switch(action.getName()) {
				case "click":
				case "tick": 
					element.click();
					break;
				case "fill": 
					StringEvaluator strEvaluator = StringEvaluator.create(environment, currentWebElement);
					String value = strEvaluator.evaluateExpression(action.getValue());
					element.sendKeys(value);
					break;
				case "select": 
					currentWebElement = element;
					break;
				}
			} else {
				info("Attention : L'action '" + action.getName() + "' n'a pas pu etre realisee car " + 
					 "l'element avec la propri�t� '" + action.getSelector().getAttribut() + " = " +
					 action.getSelector().getAttrValue() + "' n'a pas ete trouve !");
			}
		}
	}
		
	private void processForeach(Foreach loop) {
		List<WebElement> elements = processSelector(loop.getSelector());
		List<Instruction> instructions = loop.getIntructions();
		for(WebElement e : elements){
			try {
				this.currentWebElement = e;
				processInstructions(instructions);
			} 
			catch (org.openqa.selenium.StaleElementReferenceException ex) { // La page a probablement changee
				info("La page a probablement chang�: sortie de la boucle (foreach).");
				break;
			}
		}
		this.currentWebElement = null;
	}
	
	private List<WebElement> processSelector(Selector selector){
		List<WebElement> elements = new ArrayList<>();
	
		if (selector.getElement() != null) {
			elements.add(currentWebElement);
		} 
		else {
			String value = selector.getAttrValue();	
			switch(selector.getAttribut()){
			case "id" : 
				elements = driver.findElementsById(value);
				break;
			case "name" :
				elements = driver.findElements(By.name(value));
				break;
			case "content" :
				elements = driver.findElements(By.xpath("//*[@value='"+value+"']"));
				elements.addAll(driver.findElements(By.xpath("//*[text()='"+value+"']")));
				if (elements.isEmpty()) {
					elements = driver.findElements(By.xpath("//*[contains(@value,'"+value+"')]"));
					elements = driver.findElements(By.xpath("//*[contains(text(),'"+value+"')]"));
				}
				break;
			case "type" :
				elements = driver.findElements(By.tagName(value));
				if (elements.isEmpty()) {
					elements = driver.findElements(By.xpath("//*[@type='"+value+"']"));
				}
				break;
			case "title" :
				elements = driver.findElements(By.xpath("//*[@title='"+value+"']"));
				if (elements.isEmpty()) {
					elements = driver.findElements(By.xpath("//*[@alt='"+value+"']"));
				}
				break;
			case "url" :
				elements = driver.findElements(By.linkText(value));
				if (elements.isEmpty()) {
					elements = driver.findElementsByPartialLinkText(value);
				}
				break;
			}
		}
		
		return elements;
	}
	
	private void processAffectation(Affectation affectation) {
		String name;
		if (affectation.getVar() != null) {
			name = affectation.getVar().getName();
		} else {
			name = affectation.getVarRef().getName();
		}
		processAffectation(name, affectation.getExpression());
	}
	
	private void processAffectation(String name, Expression value) {
		StringEvaluator strEvaluator = StringEvaluator.create(environment, currentWebElement);
		String strValue = strEvaluator.evaluateExpression(value);
		environment.put(name, strValue);
	}

	private void processIf(If condition) {
		BooleanEvaluator eval = BooleanEvaluator.create(environment, currentWebElement);
		if (eval.evaluateIf(condition)) {
			this.processInstructions(condition.getIntructions());
		}
	}
	
	private void processWhile(While loop) {
		BooleanEvaluator eval = BooleanEvaluator.create(null, currentWebElement);
		while (eval.evaluateWhile(loop, environment)) {
			this.processInstructions(loop.getIntructions());
		}
	}
	
}