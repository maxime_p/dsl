/**
 * 
 */
package browserautomation.interpreter.evaluators;

import java.util.Map;

import org.openqa.selenium.WebElement;

import fr.emn.fila3.browserautomation.bA.BoolExpression;
import fr.emn.fila3.browserautomation.bA.Expression;
import fr.emn.fila3.browserautomation.bA.SimpleExpression;
import fr.emn.fila3.browserautomation.bA.Var;

/**
 * @author hadri
 *
 */
public class StringEvaluator {

	Map<String, String> environment;
	WebElement currentWebElement;

	public StringEvaluator(Map<String, String> environment, WebElement currentWebElement) {
		this.environment = environment;
		this.currentWebElement = currentWebElement;
	}

	public static StringEvaluator create(Map<String, String> environment, WebElement currentWebElement) {
		return new StringEvaluator(environment, currentWebElement);
	}

	public String evaluateBool(boolean bool) {
		return String.valueOf(bool);
	}
	
	public String evaluateVar(Var var) {
		if (environment.containsKey(var.getName())) {
			// return evaluateExpression(environment.get(var.getName()));
			return environment.get(var.getName());
		} else {
			throw new RuntimeException("StringEvaluator : evaluateVar => La variable " + var.getName() + " n'existe pas !");
		}
	}

	public String evaluateExpression(Expression exp) {
		if (exp instanceof SimpleExpression) {
			return evaluateSimpleExpression((SimpleExpression) exp);
		} else {
			return evaluateBoolExpression((BoolExpression) exp);
		}
	}

	private String evaluateSimpleExpression(SimpleExpression exp) {
		if (exp.getVar() != null) {
			return evaluateVar(exp.getVar());
		}
		else if (exp.getStr() != null) {
			return exp.getStr();
		}
		else if (exp.getBool() != null) {
			return exp.getBool();
		}
		else { // if (exp.getAttr() != null) {
			return evaluateAttr(exp.getAttr());
		}
	}
	
	private String evaluateBoolExpression(BoolExpression exp) {
		BooleanEvaluator boolEval = BooleanEvaluator.create(environment, currentWebElement);
		return evaluateBool(boolEval.evaluateBoolExpression(exp));
	}
	
	public String evaluateAttr(String attr) {
		String result = null;
		String alternative = null;
		
		if (currentWebElement != null) {
			switch(attr){
			case "id" : 
				result = currentWebElement.getAttribute("id");
				break;
			case "name" :
				result = currentWebElement.getAttribute("name");
				break;
			case "content" :
				result = currentWebElement.getText();
				alternative = currentWebElement.getAttribute("value");
				break;
			case "type" :
				result = currentWebElement.getTagName();
				break;
			case "title" :
				result = currentWebElement.getAttribute("title");
				alternative =  currentWebElement.getAttribute("alt");
				break;
			case "url" :
				result = currentWebElement.getAttribute("href");
				alternative = currentWebElement.getAttribute("src");
				break;
			}
		} 
		
		if (result==null || (result.isEmpty() && alternative!=null)) {
			return alternative;
		} else {
			return result;
		}
	}

}
