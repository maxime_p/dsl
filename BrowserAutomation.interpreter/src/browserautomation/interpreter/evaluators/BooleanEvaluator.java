/**
 * 
 */
package browserautomation.interpreter.evaluators;

import java.util.Map;

import org.openqa.selenium.WebElement;

import fr.emn.fila3.browserautomation.bA.BoolCondition;
import fr.emn.fila3.browserautomation.bA.BoolExpression;
import fr.emn.fila3.browserautomation.bA.Expression;
import fr.emn.fila3.browserautomation.bA.If;
import fr.emn.fila3.browserautomation.bA.SimpleExpression;
import fr.emn.fila3.browserautomation.bA.Var;
import fr.emn.fila3.browserautomation.bA.While;

/**
 * @author hadri
 *
 */
public class BooleanEvaluator {

	Map<String, String> environment;
	WebElement currentWebElement;
	
	public BooleanEvaluator(Map<String, String> environment, WebElement currentWebElement) {
		this.environment = environment;
		this.currentWebElement = currentWebElement;
	}

	public static BooleanEvaluator create(Map<String, String> environment, WebElement currentWebElement) {
		return new BooleanEvaluator(environment, currentWebElement);
	}

	public boolean evaluateString(String bool) {
		return Boolean.parseBoolean(bool);
	}

	public boolean evaluateVar(Var var) {
		if (environment.containsKey(var.getName())) {
			return evaluateString(environment.get(var.getName()));
		}
		else {
			throw new RuntimeException("BooleanEvaluator : evaluateVar => La variable " + var.getName() + " n'existe pas !");
		}
	}
	
	public boolean evaluateExpression(Expression exp) {
		if (exp instanceof SimpleExpression) {
			return evaluateSimpleExpression((SimpleExpression) exp);
		} else {
			return evaluateBoolExpression((BoolExpression) exp);
		}
	}
	
	public boolean evaluateSimpleExpression(SimpleExpression exp) {
		
		if (exp.getVar() != null) {
			return this.evaluateVar(exp.getVar());
		}
		else if (exp.getStr() != null) {
			return this.evaluateString(exp.getStr());
		}
		else if (exp.getBool() != null) {
			return this.evaluateString(exp.getBool());
		}
		else { // if (exp.getAttr() != null) {
			String strValue = StringEvaluator
								.create(environment, currentWebElement)
								.evaluateAttr(exp.getAttr());
			return this.evaluateString(strValue);
		}
		/*else {
			throwException("evaluateSimpleExpression => L'expression n'est pas valide");
			return false;
		}*/
	}

	public boolean evaluateBoolExpression(BoolExpression boolExp) {
		StringEvaluator strEval = StringEvaluator.create(environment, currentWebElement);
		String left = strEval.evaluateExpression(boolExp.getLeft());
		String right = strEval.evaluateExpression(boolExp.getRight());
		String comparator = boolExp.getComparator();
		
		if (left==null || right==null) {
			return comparator.equals("notequals") ? (left!=right) : (left==right);
		} 
		else if (comparator.equals("contains")) {
			return left.contains(right);
		} 
		else if (comparator.equals("equals")) {
			return left.equalsIgnoreCase(right);
		} 
		else { //if (comparator.equals("notequals")) {
			return !left.equalsIgnoreCase(right);
		}
	}

	public boolean evaluateWhile(While whileIns, Map<String, String> environment) {
		this.environment = environment;
		return this.evaluateCondition(whileIns.getCondition());
	}
	
	public boolean evaluateIf(If ifIns) {
		return this.evaluateCondition(ifIns.getCondition());
	}

	private boolean evaluateCondition(BoolCondition condition) {
		if (condition.getBool() != null) {
			return this.evaluateString(condition.getBool());
		} 
		else if (condition.getVar() != null){
			return this.evaluateVar(condition.getVar());
		}
		else { //if (condition.getExpression() != null){
			return this.evaluateBoolExpression(condition.getExpression());
		} 
		/*else {
			throwException("evaluateCondition => La condition n'est pas valide");
			return false;
		}*/
	}
	
}
